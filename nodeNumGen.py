## CALVR CONTAINER STARTUP SCRIPT ##
# calit2-cave-ops August 2018
#
# this script copies over each node's screen config file 
#	and starts up CalVR

import argparse
import string
import os

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', help='name of the machine to convert', required=True)
args = parser.parse_args()

s = args.hostname
output = ''.join(str(ord(c)) for c in s)

launchArgs="CalVR --host-name devcave --node-number {} --master-interface devcave-head.devcave-service --master-port 11000".format(output)

# copy screen config to config folder
os.system("cp ~/screencfg/devcave-screencfg.xml ~/calvr/config")

# start CalVR
os.system(launchArgs)
