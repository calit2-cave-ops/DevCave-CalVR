## CALVR CONTAINER STARTUP SCRIPT ## 
# calit2-cave-ops August 2018 # 
# this script copies over the head's screen config file 
#	and starts up CalVR

import string
import subprocess
import time

launchArgs="CalVR --host-name devcave"

# sleep to wait for nodes
#time.sleep(30)

while True:
     # start CalVR
     retVal=subprocess.call(launchArgs, shell=True)
     time.sleep(30)
#retVal=subprocess.call(launchArgs, shell=True)
