# !/bin/bash

# pull newest DevCave repo & move plugin dirs
cd /root/DevCave-CalVR && git pull;
mkdir /root/calvr/config/calvr_plugin_config;
cp /root/DevCave-CalVR/calvr_plugin_config/* /root/calvr/config/calvr_plugin_config;
cp /root/DevCave-CalVR/DevCave_master.xml /root/calvr/config;
cp /root/screencfg/devcave-screencfg.xml /root/calvr/config;

# run python script
#python /root/DevCave-CalVR/utils/headStart.py;

# start calvr
CalVR --host-name devcave;

echo "CalVR timed out";

sleep infinity;

exit
