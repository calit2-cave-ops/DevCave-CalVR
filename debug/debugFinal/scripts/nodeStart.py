### CALVR CONTAINER STARTUP SCRIPT ### 
## calit2-cave-ops August 2018 ## 
## this script copies over each node's screen config file 
##	and starts up CalVR

#import argparse
import string
import subprocess
import time
from random import randint

#parser = argparse.ArgumentParser()
#parser.add_argument('-n', '--hostname', help='name of the machine to convert', required=True)
#args = parser.parse_args()
#s = args.hostname

#s=subprocess.check_output("hostname")
#s = s[12:17]

#output = ''.join(str(ord(c)) for c in s)

while True:
     launchArgs="CalVR --host-name devcave --node-number {} --master-interface devcave-head.devcave-service --master-port 11000".format(randint(0,1000))
     #print(launchArgs)

     ## start CalVR
     retVal=subprocess.call(launchArgs, shell=True)
     time.sleep(30)
