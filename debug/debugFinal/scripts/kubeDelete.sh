echo "deleting all CalVR pods, daemonset, services...";
kubectl delete pod devcave-head;
kubectl delete daemonset devcave-pod;
kubectl delete service devcave-service;
echo "done.";
