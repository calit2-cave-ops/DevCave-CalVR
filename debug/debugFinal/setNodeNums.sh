# usage: source setNodeNums.sh

# create a new env variable to launch calvr with
input=$(hostname)
input=${input:(-5)}
NODENUM=()
for i in $(seq 0 ${#input})
    do array[$i]=${input:$i:1}
    NODENUM+=$(printf "%d" \'${array[$i]})
done
export NODENUM
exit
