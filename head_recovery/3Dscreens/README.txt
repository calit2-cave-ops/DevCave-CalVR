How to run:

source ./env/bin/activate	# loads local python environment for this tool
cd LGWebOSRemote
python ./go3D.py		# switches to 3D mode
python ./go2D.py		# switches to 2D mode

Can also run commands on individual screens:

python ./lgtv.py <IP address goes here> get3DStatus	# queries state
python ./lgtv.py <IP address goes here> input3DOff	# turns off 3D
python ./lgtv.py <IP address goes here> mouse23D	# turns on 3D


(Install notes)
virtualenv env				# create new local environment
source ./env/bin/activate		# load local environment settings
pip install -r LGWebOSRemote/requirements.txt	# local install libraries
cp suncave.json ~/.lgtv.json		# TV key database file for this system

To auth:
python ./lgtv.py <IP address goes here> auth
# use remote to say yes on the tv
# ~/.lgtv.json should be updated, test with go3D or go2D

To reauth:
Delete the IP address entry from ~/.lgtv.json and reauth
